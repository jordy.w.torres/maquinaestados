----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:22:51 05/18/2022 
-- Design Name: 
-- Module Name:    Estado - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Estado is
    Port (clk: in std_logic;
	       ing :in std_logic;
	       sok: in std_logic;
			 play: in std_logic;
			 pause: in std_logic;
			 stop: in std_logic;
			 eject: in std_logic;
          bad_dvd: out std_logic;
			 exp_eject :out std_logic;
			 e: inout std_logic_vector(0 to 2));
			 
end Estado;

architecture Behavioral of Estado is
type estado is A,B,C,D,E,F;
signal estado,estado_act,estado_fut
begin
  process (estado_act,eject,play,pause,sok)
  begin
   case estado_act is
	when A =>
	       bad_dvd <= '0';
          exp_eject <='0';
           e <= "000";
           if eject = '1' then
			    estado_fut <= B;
				 else 
				 estado_fut <=A ;
				 end if;
	 when B => 
			  e <="001";
			  if ing ='1' then
			  estado_fut <= C;
           else
           estado_fut <= B;			  
	        end if;
	  when C =>
	        e<= "010";
        	 if sok = '1' then
			 estado_fut <= D;
			 else
			 estado_fut <= C;
			 end if;
	  when D=>
	        e <="011";
			if play = '1' then
			estado_fut <=E;
			else
			estado_fut <=D;
	  when E => 
	     
	       e<="100";
			 if pausa = '1' then
			 estado_fut<=F;
			 else
			 estado_fut<= E;
	 when F=>
	       e<= "101";
			 if stop = '1' then
			 estado_fut<=D;
			 else
			 estado_fut<=A;
end process;			  

end Behavioral;



